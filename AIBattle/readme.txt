
Check out this overview of the project and review the AI Battle project PDF for more helpful info:
https://youtu.be/N5o_EfZd6O0


New this time around:
* Your AI can check the remaining duration of a bomb
* Your AI can check the names of adjacent agents


To add your AI into the battle:
1. Write your new AI script, using TestAI or RandomAI as examples
2. Create a scriptable object for your AI
3. Attach the scriptable object to the GameManager object in the list of combatants
4. Hit play

The game will start paused. To unpause the game, uncheck the Paused bool on the GameManager game object.

This is the exact project we will be using to battle our AI, but feel free to modify your own version for testing. For example, you can modify the code in the OnDrawGizmos function to draw any information your AI script might be currently storing.

Good luck!