using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ShopItem : MonoBehaviour
{
    private int currentShop;

    private int currentItem;

    [SerializeField]
    private TextMeshProUGUI moneyText;

    private List<Consumable> consumableList;

    private List<Equipment> equipmentList;

    [SerializeField]
    private List<GameObject> UIList;

    private TextMeshProUGUI[] UIChildren;

    private void OnEnable()
    {
        currentShop = 0;
        currentItem = 0;
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        moneyText.text = GameManager.instance.coin.ToString();

        // change selection
        if (Input.GetKeyDown(KeyCode.UpArrow) && currentItem > 0)
        {
            currentItem--;
        }
        if (Input.GetKeyDown(KeyCode.DownArrow) && currentItem < UIList.Count)
        {
            currentItem++;
        }

        // change shop
        if (Input.GetKeyDown(KeyCode.LeftArrow) && currentShop > 0)
        {
            currentShop--;
            currentItem = 0;
        }
        if (Input.GetKeyDown(KeyCode.RightArrow) && currentShop < 1)
        {
            currentShop++;
            currentItem = 0;
        }

        // consumablel shop
        // not done!!!
        if (currentShop == 0)
        {
            for (int i = 0; i < UIList.Count; i++)
            {
                UIChildren = UIList[i].GetComponentsInChildren<TextMeshProUGUI>();
                if (i == currentItem)
                {
                    UIChildren[0].color = Color.blue;
                }
                else if (i != currentItem)
                {
                    UIChildren[0].color = Color.black;
                }
                UIChildren[0].text = consumableList[i].itemName;
                UIChildren[1].text = (consumableList[i].cost).ToString();
            }

            if (Input.GetKeyDown(KeyCode.Return) && GameManager.instance.coin >= consumableList[currentItem].cost)
            {
                GameManager.instance.inventory.AddConsumable(consumableList[currentItem], 1);
                GameManager.instance.coin -= consumableList[currentItem].cost;
            }
        }

        // equipment shop
        // not done!!!
        if (currentShop == 1)
        {
            for (int i = 0; i < UIList.Count; i++)
            {
                UIChildren = UIList[i].GetComponentsInChildren<TextMeshProUGUI>();
                if (i == currentItem)
                {
                    UIChildren[0].color = Color.blue;
                }
                else if (i != currentItem)
                {
                    UIChildren[0].color = Color.black;
                }
                UIChildren[0].text = equipmentList[i].itemName;
                UIChildren[1].text = (equipmentList[i].cost).ToString();
            }

            if (Input.GetKeyDown(KeyCode.Return) && GameManager.instance.coin >= equipmentList[currentItem].cost)
            {
                GameManager.instance.inventory.AddEquipment(equipmentList[currentItem], 1);
                GameManager.instance.coin -= equipmentList[currentItem].cost;
            }
        }
    }

    public void GetItems(List<Consumable> consumables, List<Equipment> equipments)
    {
        consumableList = consumables;
        equipmentList = equipments;
    }
}
