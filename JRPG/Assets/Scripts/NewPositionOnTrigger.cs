using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewPositionOnTrigger : MonoBehaviour
{
    public Vector3 position;

    public Quaternion rotation;
}
