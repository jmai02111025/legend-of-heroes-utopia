using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestManager : MonoBehaviour
{
    public List<Quest> quest = new List<Quest>();

    public bool canView;

    [SerializeField]
    private GameObject requestBoardUI;

    // Update is called once per frame
    void Update()
    {
        if (canView == true)
        {
            if (Input.GetButtonDown("Submit"))
            {
                requestBoardUI.SetActive(true);
                GameManager.instance.pc.enabled = false;
            }
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            canView = true;
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            canView = false;
        }
    }
}
