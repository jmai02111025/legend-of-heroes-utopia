using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
//using UnityEditorInternal;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public enum BattleState
{
    Start,
    TurnRoll,
    PlayerAction,
    PlayerMove,
    EnemyMove,
    EnemyAttack,
    Result,
}

public class BattleSystem : MonoBehaviour
{
    #region Initial Variables

    [SerializeField]
    StatusEffectBattle statusEffectBattle;

    bool endOfBattle = false;

    public bool selectionMade = false;
    public bool magicSelected = false;

    public bool enemyDied = false;

    public Dictionary<int, CharacterInformation> listOfAllCharacters = new Dictionary<int, CharacterInformation>();

    List<CharacterInformation> allCharactersBeforeSorting = new List<CharacterInformation>();

    public List<Character> playerUnits;
    public Character[] playerUnitPosition = new Character[9];

    public List<Character> enemyUnits = new List<Character>();
    public Character[] enemyUnitPosition = new Character[9];

    public List<GameObject> characterHuds;

    public List<GameObject> actionButton;

    public List<Text> actionText;

    public BattleState state;

    [SerializeField]
    private GameObject magicPanel;

    [SerializeField]
    GameObject itemPanel;

    public GameObject actionSeletor;

    public int currentSelection;

    public List<GameObject> battleSprites;

    public List<GameObject> enemyBattleSprites;

    public List<GameObject> playerBattleSprites;

    public Vector3[] playerSpawnPositions;

    public Vector3[] enemySpawnPositions;

    [SerializeField]
    private List<int> tempList;

    public GameObject turnIndicator;

    public GameObject targetSelector;

    public int moveIndex;

    private bool eMoved;

    private bool pMoved;

    private Transform parent;

    private int currentTarget;

    private bool magicAttack;

    public delegate void StateChange(BattleState battleStatus);
    StateChange stateChange;

    [SerializeField]
    TargetSelection targetSelection;

    public Sprite unselectedButton;

    public Sprite selectedButton;

    public delegate void BeginingOfTurn(int whichCharacter, int whichAlly);
    public BeginingOfTurn beginingOfTurn;
    #endregion

    public List<CharacterInformation> BubbleSort(List<CharacterInformation> allCharacters)
    {
        bool noChanges = true;
        int numberofLoops = 0;
        while (noChanges == true && numberofLoops < 1000)
        {
            noChanges = false;
            numberofLoops += 1;
            for (int i = 0; i < allCharacters.Count - 1; i++)
            {
                if (allCharacters[i].characterInformation.Spd < allCharacters[i + 1].characterInformation.Spd)
                {
                    noChanges = true;
                    CharacterInformation tempCharacter = allCharacters[i];
                    allCharacters[i] = allCharacters[i + 1];
                    allCharacters[i + 1] = tempCharacter;
                }
            }
        }

        return allCharacters;
    }
    private void OnEnable()
    {
        stateChange += ChangeState;
        playerUnits = GameManager.instance.Characters;
        CreateEnemy();
        state = BattleState.Start;
        StartCoroutine(SetUpBattle());
    }

    #region State Functions
    void CheckPlayersHP()
    {
        // destroy the character when the character's hp is less than 0
        for (int i = 0; i < playerUnits.Count; i++)
        {
            if (playerUnits[i] != null)
            {
                if (playerUnits[i].HP <= 0)
                {
                    Destroy(playerBattleSprites[i]);
                }
            }
        }
    }
    public void CheckEnemiesHP()
    {
        // destroy the character when the character's hp is less than 0
        for (int i = 0; i < enemyUnits.Count; i++)
        {
            if (enemyUnits[i] != null)
            {
                if (enemyUnits[i].HP <= 0)
                {
                    if (enemyDied == true)
                    {
                        if (moveIndex == listOfAllCharacters.Count - 1)
                        {
                            moveIndex = 0;
                        }
                        else
                        {
                            moveIndex += 1;
                        }
                        enemyDied = false;
                    }
                    Destroy(enemyBattleSprites[i]);
                }
            }
        }

    }

    void BattleResult()
    {
        // result screen
        if (state == BattleState.Result)
        {
            if (Input.GetKeyDown(KeyCode.Return))
            {
                GameManager.instance.destroyEnemy = true;
                SceneManager.LoadScene("Scene1");
            }
        }
    }

    void EnemyMove()
    {
        //If it is not the result state, then continue. 
        if (state != BattleState.Result)
        {
            //Set the magic attack to false, and select a random character within the players team to target
            magicAttack = false;
            currentTarget = Random.Range(0, playerUnits.Count);
            //Loop through the player units, until one is found that is still alive. Find the closest player.

            for (int i = 2; i >= 0; i--)
            {
                if (playerUnitPosition[i] != null && playerUnitPosition[i].HP > 0)
                {
                    currentTarget = i;
                    break;
                }
                else if (playerUnitPosition[i + 3] != null && playerUnitPosition[i + 3].HP > 0)
                {
                    currentTarget = i + 3;
                    break;
                }
                else if (playerUnitPosition[i + 6] != null && playerUnitPosition[i + 6].HP > 0)
                {
                    currentTarget = i + 6;
                    break;
                }
            }
            Debug.Log("Character selected: " + playerUnitPosition[currentTarget].Base.CharacterName + " HP: " + playerUnitPosition[currentTarget].HP);

            bool isConfused = true;
            bool isAlly = true;
            int numberOfLoops = 0;
            while (isConfused && numberOfLoops < 100 || isAlly && numberOfLoops < 100)
            {
                numberOfLoops += 1;
                isConfused = false;
                isAlly = false;

                //loop through all of the enemy characters, until one is found that is alive
                if (moveIndex > listOfAllCharacters.Count - 1)
                {
                    moveIndex = 0;
                }
                if (listOfAllCharacters[moveIndex].characterSprite == null)
                {
                    moveIndex += 1;
                }
                if (moveIndex > listOfAllCharacters.Count)
                {
                    moveIndex = 0;
                }
                if (listOfAllCharacters[moveIndex].isAlly)
                {
                    isAlly = true;
                    moveIndex += 1;
                }
                if (moveIndex > listOfAllCharacters.Count - 1)
                {
                    moveIndex = 0;
                }
                if (listOfAllCharacters[moveIndex].characterInformation.isConfused)
                {
                    isConfused = true;
                    moveIndex += 1;
                }
            }
            //Begin the attack coroutine
            StartCoroutine(Attack(listOfAllCharacters[moveIndex].characterSprite, playerSpawnPositions[currentTarget], 1.5f));
            eMoved = true;
        }
    }
    #endregion

    //This is a function that will be used to change the states and certain variables that do and don't apply to them.
    void ChangeState(BattleState battleStatus)
    {
        switch (battleStatus)
        {
            case BattleState.Start:
                actionSeletor.SetActive(false);
                break;
            case BattleState.TurnRoll:
                eMoved = false;
                state = BattleState.TurnRoll;
                // decide who's turn is next
                NextMove();
                StartCoroutine(WaitForFrameToEnd());
                break;
            case BattleState.PlayerAction:
                // player selection time
                state = BattleState.PlayerAction;
                if (eMoved == false)
                {
                    eMoved = true;
                    int currentPlayerCharacter = currentSelection;
                    while (listOfAllCharacters[currentPlayerCharacter].characterInformation.characterInt > 0)
                    {
                        currentPlayerCharacter += 1;
                    }
                    //At the start of the enemies turn, go and activate any status effects, and then check to see if any of them died, and if all of them died, activate the win state
                    beginingOfTurn(listOfAllCharacters[moveIndex].characterInformation.characterInt, currentPlayerCharacter);
                    CheckPlayersHP();
                    CheckWin();
                }
                ActionSelection();
                break;
            case BattleState.PlayerMove:
                state = BattleState.PlayerMove;
                actionSeletor.SetActive(false);
                break;
            case BattleState.EnemyMove:
                state = BattleState.EnemyAttack;
                actionSeletor.SetActive(false);
                if (eMoved == false)
                {
                    eMoved = true;
                    //At the start of the enemies turn, go and activate any status effects, and then check to see if any of them died, and if all of them died, activate the win state
                    beginingOfTurn(listOfAllCharacters[moveIndex].characterInformation.characterInt, 0);
                    CheckEnemiesHP();
                    CheckWin();
                    StartCoroutine(WaitBetweenEnemies());
                }
                break;
            case BattleState.Result:
                actionSeletor.SetActive(false);
                BattleResult();
                break;
        }
    }

    private void Update()
    {
        if (endOfBattle == true)
        {
            state = BattleState.Result;
        }
        selectionMade = false;
        //Call the current state every frame
        stateChange(state);

        // choose target
        if (targetSelector.GetComponent<TargetSelection>().finishedSelection == true && state == BattleState.PlayerMove && pMoved == false)
        {
            currentSelection = 0;
            targetSelector.SetActive(false);
            currentTarget = targetSelector.GetComponent<TargetSelection>().GetTarget();
            int currentMagic = magicPanel.GetComponent<MagicPanel>().GetMagic();
            WhichTarget tempTarget = WhichTarget.Enemies;

            if (magicAttack)
            {
                tempTarget = listOfAllCharacters[moveIndex].characterInformation.Magics[currentMagic].Base.whichTarget;
            }


            if (tempTarget == WhichTarget.Enemies)
            {
                StartCoroutine(Attack(listOfAllCharacters[moveIndex].characterSprite, new Vector3(enemyUnits[currentTarget].xPos, enemyUnits[currentTarget].yPos, 0f), 1.5f));

            }
            else if (tempTarget == WhichTarget.Allies)
            {
                StartCoroutine(Attack(listOfAllCharacters[moveIndex].characterSprite, new Vector3(playerUnits[currentTarget].xPos, playerUnits[currentTarget].yPos, 0f), 1.5f));
            }

            pMoved = true;
        }
    }

    // set up battle, including icons, health bars, and mana bars
    public IEnumerator SetUpBattle()
    {
        int numberOfCharacters = 0;
        moveIndex = -1;

        string[] pos = GameManager.instance.battlePosition;

        #region Spawn Player Characters
        for (int i = 0; i < playerUnits.Count; i++)
        {
            for (int j = 0; j < pos.Length; j++)
            {
                if (pos[j] == playerUnits[i].Base.CharacterName)
                {
                    // create prefabs of character battle sprite
                    GameObject PlayerSprite = Instantiate(playerUnits[i].Base.CharacterPrefab, playerSpawnPositions[j], Quaternion.identity);
                    parent = GameObject.Find("CharacterCanvas").transform;
                    PlayerSprite.transform.SetParent(parent.transform, false);
                    PlayerSprite.transform.localScale = new Vector3(20f, 20f, 20f);
                    PlayerSprite.GetComponent<Animator>().SetBool("BattleIdle", true);
                    battleSprites.Add(PlayerSprite);

                    playerBattleSprites.Add(PlayerSprite);
                    playerUnitPosition[j] = playerUnits[i];

                    // create prefabs of character hud, then set them to the correct stats.
                    GameObject characterHud = Instantiate(playerUnits[i].Base.CharacterHud, new Vector3(-1200f + i * 700f, -750f, 0f), Quaternion.identity);
                    characterHud.transform.SetParent(parent.transform, false);
                    characterHud.GetComponent<CharacterHud>().SetData(playerUnits[i]);
                    characterHuds.Add(characterHud);

                    CharacterInformation tempCharacterInformation = new CharacterInformation();
                    tempCharacterInformation.characterSprite = PlayerSprite;
                    tempCharacterInformation.characterHud = characterHud;
                    tempCharacterInformation.characterInformation = playerUnits[i];
                    tempCharacterInformation.spawnPosition = playerSpawnPositions[j];
                    tempCharacterInformation.isAlly = true;
                    playerUnits[i].characterInt = -i - 1;

                    statusEffectBattle.listOfCharacters.Add(playerUnits[i].characterInt, playerUnits[i]);

                    allCharactersBeforeSorting.Add(tempCharacterInformation);

                    //statusEffectBattle.listOfCharacters.Add(numberOfCharacters, playerUnits[i]);
                    numberOfCharacters += 1;
                }
            }

        }
        #endregion
        int[] enemyPos =
        {
            0, 0, 0,
            0, 0, 0,
            0, 0, 0
        };

        #region Spawn Enemy Characters
        for (int i = 0; i < enemyUnits.Count; i++)
        {
            //enemyUnitPosition[i] = enemyUnits[i];'
            int newPos = -1;
            while (true)
            {
                newPos = Random.Range(0, 8);
                if (enemyPos[newPos] == 0)
                {
                    enemyPos[newPos] = 1;

                    // create prefabs of enemy battle sprite
                    GameObject EnemySprite = Instantiate(enemyUnits[i].Base.CharacterPrefab, enemySpawnPositions[newPos], Quaternion.identity);
                    EnemySprite.transform.SetParent(parent.transform, false);
                    battleSprites.Add(EnemySprite);

                    enemyBattleSprites.Add(EnemySprite);


                    targetSelection.numberOfEnemies += 1;

                    enemyUnits[i].characterInt = i + 1;
                    CharacterInformation tempCharacterInformation = new CharacterInformation();
                    tempCharacterInformation.characterInformation = enemyUnits[i];
                    tempCharacterInformation.spawnPosition = enemySpawnPositions[newPos];
                    tempCharacterInformation.characterSprite = EnemySprite;
                    tempCharacterInformation.characterInformation.xPos = enemySpawnPositions[newPos].x;
                    tempCharacterInformation.characterInformation.yPos = enemySpawnPositions[newPos].y;

                    allCharactersBeforeSorting.Add(tempCharacterInformation);

                    statusEffectBattle.listOfCharacters.Add(i + 1, enemyUnits[i]);
                    numberOfCharacters += 1;
                    tempCharacterInformation.isAlly = false;

                    break;
                }
            }




        }
        #endregion

        //bubble sort all of the characters, and put them in order from fastest to slowest
        List<CharacterInformation> tempAllCharacterInformation = BubbleSort(allCharactersBeforeSorting);

        for (int i = 0; i < tempAllCharacterInformation.Count; i++)
        {
            listOfAllCharacters.Add(i, tempAllCharacterInformation[i]);
        }

        // wait 1.5 seconds for characters to finish their intro animation
        yield return new WaitForSeconds(1.5f);


        stateChange(BattleState.TurnRoll);
    }

    // player choosing actions
    private void ActionSelection()
    {
        actionButton[currentSelection].GetComponent<Image>().sprite = selectedButton;
        actionText[currentSelection].GetComponent<Text>().color = Color.Lerp(Color.white, Color.yellow, Mathf.PingPong(Time.time, 1));
        if (Input.GetKeyUp(KeyCode.DownArrow) && selectionMade == false)
        {
            selectionMade = true;
            if (currentSelection < 3)
            {
                actionButton[currentSelection].GetComponent<Image>().sprite = unselectedButton;
                actionText[currentSelection].GetComponent<Text>().color = Color.white;
                currentSelection++;
            }
        }
        //Runs the function if the player pressed the down arrow, and a selection has not ben made
        else if (Input.GetKeyUp(KeyCode.UpArrow) && selectionMade == false)
        {
            selectionMade = true;
            if (currentSelection > 0)
            {
                actionButton[currentSelection].GetComponent<Image>().sprite = unselectedButton;
                actionText[currentSelection].GetComponent<Text>().color = Color.white;
                currentSelection--;
            }
        }

        // open and show panels
        if (Input.GetKeyUp(KeyCode.Return) && selectionMade == false && magicSelected == false)
        {
            magicSelected = true;
            selectionMade = true;
            actionText[currentSelection].GetComponent<Text>().color = Color.white;
            actionButton[currentSelection].GetComponent<Image>().sprite = unselectedButton;
            // attack
            if (currentSelection == 0)
            {
                targetSelector.GetComponent<TargetSelection>().attack = true;
                magicAttack = false;
                turnIndicator.SetActive(false);
                actionSeletor.SetActive(false);
                targetSelector.SetActive(true);
            }
            // magic panel
            if (currentSelection == 1)
            {
                int currentMagic = magicPanel.GetComponent<MagicPanel>().GetMagic();
                targetSelector.GetComponent<TargetSelection>().target = listOfAllCharacters[moveIndex].characterInformation.Magics[currentMagic].Base.whichTarget;
                magicAttack = true;
                magicPanel.SetActive(true);
                actionSeletor.SetActive(false);
            }
            // item panel
            if (currentSelection == 2)
            {
                targetSelector.GetComponent<TargetSelection>().attack = false;
                magicAttack = false;
                itemPanel.SetActive(true);
                actionSeletor.SetActive(false);
            }
            // run
            if (currentSelection == 3)
            {
                for (int i = 0; i < playerUnits.Count; i++)
                {
                    playerUnits[i].HP = Mathf.FloorToInt(0.9f * playerUnits[i].HP);
                    playerUnits[i].MP = Mathf.FloorToInt(0.9f * playerUnits[i].MP);
                }
                SceneManager.LoadScene("Scene1");
                GameManager.instance.destroyEnemy = false;
            }
        }
        // close and hide panels
        if (Input.GetKeyUp(KeyCode.Escape))
        {
            magicPanel.SetActive(false);
            actionSeletor.SetActive(true);
            itemPanel.SetActive(false);
            magicSelected = false;

            if (targetSelector.activeInHierarchy == true)
            {
                if (magicAttack == true)
                {
                    magicPanel.SetActive(true);
                }
                if (magicAttack == false)
                {
                    actionSeletor.SetActive(true);
                }
                targetSelector.SetActive(false);
                turnIndicator.SetActive(true);
            }
        }
        CheckWin();
    }

    // attack animation
    private IEnumerator Attack(GameObject go, Vector3 targetPosition, float seconds)
    {
        Debug.Log(targetPosition);
        if (go == null)
        {
            NextMove();
        }
        // go to target position
        go.GetComponent<Animator>().SetBool("BattleIdle", false);

        float elapsedTime = 0;
        Vector3 startingPosition = go.transform.localPosition;
        Animator animator = go.GetComponent<Animator>();
        AnimationClip[] clips = animator.runtimeAnimatorController.animationClips;
        if (magicAttack)
        {
            int magic = magicPanel.GetComponent<MagicPanel>().GetMagic();
            Debug.Log(playerUnits[moveIndex].Magics[magic].Base.name);
        }

        while (elapsedTime < seconds)
        {
            go.GetComponent<Animator>().SetBool("isRunning", true);
            go.transform.localPosition = Vector3.Lerp(startingPosition, targetPosition, (elapsedTime / seconds));
            elapsedTime += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }

        go.GetComponent<Animator>().SetBool("isRunning", false);
        go.GetComponent<Animator>().SetBool("isAttacking", true);

        int animIndex = -1;
        for (int i = 0; i < clips.Length; i++) // look for attack animation
        {
            string[] name = clips[i].name.Split(' ');
            for (int j = 0; j < name.Length; j++)
            {
                if (name[j] == "Attack")
                {
                    animIndex = i;
                }
            }
        }
        if (animIndex != -1) // found attack animation
        {
            float time = clips[animIndex].length;
            yield return new WaitForSeconds(time);
        }
        go.GetComponent<Animator>().SetBool("isAttacking", false);

        // calculate damage
        // normal attack
        if (magicAttack == false)
        {
            if (state == BattleState.EnemyAttack)
            {
                playerUnits[currentTarget].TakeAttackDamage(listOfAllCharacters[moveIndex].characterInformation);
                GetComponent<Familiarity>().UpdateDefFamiliarity(playerUnits[currentTarget]);
                characterHuds[currentTarget].GetComponent<CharacterHud>().SetData(playerUnits[currentTarget]);
            }
            if (state == BattleState.PlayerMove)
            {
                GetComponent<Familiarity>().UpdateStrFamiliarity(listOfAllCharacters[moveIndex].characterInformation);
                enemyUnits[currentTarget].TakeAttackDamage(listOfAllCharacters[moveIndex].characterInformation);
            }
            //Sets the magic selected to false
            magicSelected = false;
        }

        bool playerEffect = false;

        // magic or skill attack
        if (magicAttack == true)
        {
            if (state == BattleState.PlayerMove)
            {
                #region Player attacks depending on the attack direction
                int currentMagic = magicPanel.GetComponent<MagicPanel>().GetMagic();
                GetComponent<Familiarity>().UpdateMgsFamiliarity(listOfAllCharacters[moveIndex].characterInformation);
                GetComponent<Familiarity>().updateMagicFamiliarity(listOfAllCharacters[moveIndex].characterInformation.Magics[currentMagic]);
                if (listOfAllCharacters[moveIndex].characterInformation.Magics[currentMagic].Base.attackDirection == AttackDirection.Single)
                {
                    listOfAllCharacters[moveIndex].characterHud.GetComponent<CharacterHud>().SetData(listOfAllCharacters[moveIndex].characterInformation);
                    //Sets the magic selected to false
                    magicSelected = false;
                    WhichTarget tempTarget = listOfAllCharacters[moveIndex].characterInformation.Magics[currentMagic].Base.whichTarget;

                    if (tempTarget == WhichTarget.Enemies)
                    {
                        enemyUnits[currentTarget].TakeSkillDamage(listOfAllCharacters[moveIndex].characterInformation.Magics[currentMagic], listOfAllCharacters[moveIndex].characterInformation);
                        statusEffectBattle.AssignNewStatusEffect(listOfAllCharacters[moveIndex].characterInformation.Magics[currentMagic].Base.statusCondition, enemyUnits[currentTarget].characterInt);
                    }
                    else if (tempTarget == WhichTarget.Allies)
                    {
                        statusEffectBattle.AssignNewStatusEffect(listOfAllCharacters[moveIndex].characterInformation.Magics[currentMagic].Base.statusCondition, playerUnits[currentTarget].characterInt);
                        playerEffect = true;
                    }
                }
                #region Unused Code
                /* if (listOfAllCharacters[moveIndex].characterInformation.Magics[currentMagic].Base.attackDirection == AttackDirection.LineHorizontal)
                 {
                     for (int i = 0; i < enemyUnits.Count; i++)
                     {
                         if(enemyUnits[i] != null)
                         {
                             if (enemyUnits[i].xPos == enemySpawnPositions[0].x || enemyUnits[i].xPos == enemySpawnPositions[1].x)
                             {
                                 enemyUnits[i].TakeMagicDamage(listOfAllCharacters[moveIndex].characterInformation.Magics[currentMagic], listOfAllCharacters[moveIndex].characterInformation);
                                 listOfAllCharacters[moveIndex].characterHud.GetComponent<CharacterHud>().SetData(listOfAllCharacters[moveIndex].characterInformation);
                                 //Sets the magic selected to false
                                 magicSelected = false;
                                 statusEffectBattle.AssignNewStatusEffect(listOfAllCharacters[moveIndex].characterInformation.Magics[currentMagic].Base.statusCondition, enemyUnits[i].characterInt);
                             }
                         }
                     }
                 }
                 if (listOfAllCharacters[moveIndex].characterInformation.Magics[currentMagic].Base.attackDirection == AttackDirection.LineVertical)
                 {
                     for (int i = 0; i < enemyUnits.Count; i++)
                     {
                         if (enemyUnits[i] != null)
                         {
                             if (enemyUnits[i].yPos == enemySpawnPositions[0].y)
                             {
                                 enemyUnits[i].TakeMagicDamage(listOfAllCharacters[moveIndex].characterInformation.Magics[currentMagic], listOfAllCharacters[moveIndex].characterInformation);
                                 listOfAllCharacters[moveIndex].characterHud.GetComponent<CharacterHud>().SetData(listOfAllCharacters[moveIndex].characterInformation);
                                 //Sets the magic selected to false
                                 magicSelected = false;
                                 statusEffectBattle.AssignNewStatusEffect(listOfAllCharacters[moveIndex].characterInformation.Magics[currentMagic].Base.statusCondition, enemyUnits[i].characterInt);
                             }
                         }
                     }
                 }
                 if (listOfAllCharacters[moveIndex].characterInformation.Magics[currentMagic].Base.attackDirection == AttackDirection.All)
                 {
                     for (int i = 0; i < enemyUnits.Count; i++)
                     {
                             if (enemyUnits[i].yPos == enemySpawnPositions[0].y)
                             {
                                 enemyUnits[i].TakeMagicDamage(listOfAllCharacters[moveIndex].characterInformation.Magics[currentMagic], listOfAllCharacters[moveIndex].characterInformation);
                                 listOfAllCharacters[moveIndex].characterHud.GetComponent<CharacterHud>().SetData(listOfAllCharacters[moveIndex].characterInformation);
                                 //Sets the magic selected to false
                                 magicSelected = false;
                                 statusEffectBattle.AssignNewStatusEffect(listOfAllCharacters[moveIndex].characterInformation.Magics[currentMagic].Base.statusCondition, enemyUnits[i].characterInt);
                         }
                     }
                 }*/
                #endregion
                #endregion
            }
            if (state == BattleState.EnemyMove)
            {
                GetComponent<Familiarity>().UpdateMdfFamiliarity(playerUnits[currentTarget]);
            }
        }

        if (state == BattleState.PlayerMove && !playerEffect)
        {
            if (enemyUnits[currentTarget].HP <= 0)
            {
                GetComponent<Familiarity>().UpdateWeaponFamiliarity(listOfAllCharacters[moveIndex].characterInformation);
                // quest target defeated

                for (int j = 0; j < GameManager.instance.acceptedQuests.Count; j++)
                {
                    if (enemyUnits[currentTarget].Base.NpcType == GameManager.instance.acceptedQuests[j].TargetType)
                    {
                        if (GameManager.instance.acceptedQuests[j].amount > 0)
                        {
                            GameManager.instance.acceptedQuests[j].amount--;
                        }
                        else
                        {
                            GameManager.instance.acceptedQuests[j].amount = 0;
                        }
                    }
                }
            }
        }

        CheckEnemiesHP();

        // go back to starting position
        elapsedTime = 0;
        go.GetComponent<Animator>().SetBool("isJumping", true);
        yield return new WaitForSeconds(0.4f);
        while (elapsedTime < 0.6f)
        {
            go.transform.localPosition = Vector3.Lerp(targetPosition, startingPosition, (elapsedTime / 0.6f));
            elapsedTime += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
        go.GetComponent<Animator>().SetBool("isJumping", false);
        go.GetComponent<Animator>().SetBool("BattleIdle", true);

        CheckWin();

        // roll next turn if the battle is not over
        if (state != BattleState.Result)
        {
            state = BattleState.TurnRoll;
        }
    }

    // determine the next character to move
    public void NextMove()
    {
        //At the beginning of every turn, cycle through to the next character in the loop
        moveIndex += 1;

        if (moveIndex == listOfAllCharacters.Count)
        {
            moveIndex = 0;
        }
        if (listOfAllCharacters[moveIndex].characterSprite == null && listOfAllCharacters[moveIndex].characterInformation.HP <= 0)
        {
            if (moveIndex == listOfAllCharacters.Count)
            {
                moveIndex = 0;
            }
            else
            {
                moveIndex += 1;
            }
        }

        bool isPlayer = false;

        for (int i = 0; i < playerUnits.Count; i++)
        {
            if (listOfAllCharacters[moveIndex].characterInformation.Base.name == playerUnits[i].Base.name)
            {
                isPlayer = true;
            }
        }

        if (isPlayer == true)
        {
            pMoved = false;
            actionSeletor.SetActive(true);
            turnIndicator.SetActive(true);
            turnIndicator.transform.localPosition = listOfAllCharacters[moveIndex].spawnPosition + new Vector3(0f, 220f, 0f);
            stateChange(BattleState.PlayerAction);
        }
        // otherwise it would be the enemy's turn which attacks a random character
        else
        {
            eMoved = false;
            stateChange(BattleState.EnemyMove);
        }
    }

    // victory menu
    public void CheckWin()
    {
        int deadEnemy = 0;
        int deadPlayer = 0;

        for (int i = 0; i < playerUnits.Count; i++)
        {
            if (playerUnits[i].HP <= 0)
            {
                deadPlayer++;
            }
        }

        for (int i = 0; i < enemyUnits.Count; i++)
        {
            if (enemyUnits[i].HP <= 0)
            {
                deadEnemy++;
            }
        }

        // if all enemies are killed, exit out of the battle world
        if (enemyUnits.Count == deadEnemy)
        {
            state = BattleState.Result;
        }

        // if all players are killed, go back to main menu (just for now)
        if (playerUnits.Count == deadPlayer)
        {
            SceneManager.LoadScene("TitleScene");
        }
    }

    public void SetCharacters(List<Character> characters)
    {
        playerUnits = characters;
    }

    private void CreateEnemy()
    {
        // add enemy characters to the battle scene
        for (int i = 0; i < GameManager.instance.EBase.Count; i++)
        {
            enemyUnits.Add(new Character(GameManager.instance.EBase[i]));
        }
    }
    public IEnumerator WaitBetweenEnemies()
    {
        yield return new WaitForSeconds(1);
        EnemyMove();
    }
    public IEnumerator WaitBetweenAllies()
    {
        yield return new WaitForSeconds(1);
    }
    public IEnumerator WaitForFrameToEnd()
    {
        yield return new WaitForSeconds(1);
        selectionMade = false;
        magicSelected = false;
    }
}
