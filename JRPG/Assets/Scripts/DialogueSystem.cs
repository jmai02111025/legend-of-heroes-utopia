using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogueSystem : MonoBehaviour
{
    public DialogueManager dm;

    [SerializeField]
    private List<GameObject> characterIconBox;

    [SerializeField]
    private Text dialogueText;

    [SerializeField]
    private GameObject dialogueBackground;

    [SerializeField]
    private Text NameTextBox;

    private int index;

    private bool startDialogue;

    private bool finishText;

    private bool textDisplaying;

    [SerializeField]
    private QuestManager qm;

    [SerializeField]
    private Sprite loadingImage;

    [SerializeField]
    private Sprite doneImage;

    [SerializeField]
    private GameObject dialogueTextBox;

    private int currentSpeaker;
    private int numSpeakers;

    private void OnEnable()
    {
        if (qm.quest.Contains(dm.quest) == false)
        {
            dm.DState = DialogueManager.DialogueState.QuestInfo;
        }

        // set the state to normal if the quest is accepted but not yet completed
        for (int i = 0; i < GameManager.instance.acceptedQuests.Count; i++)
        {
            if (GameManager.instance.acceptedQuests[i].questName == dm.quest.questName)
            {
                dm.DState = DialogueManager.DialogueState.Normal;
                break;
            }
        }

        // set state to quest complete if the quest is completed and reward the player
        for (int i = 0; i < GameManager.instance.acceptedQuests.Count; i++)
        {
            if (GameManager.instance.acceptedQuests[i].amount == 0 && GameManager.instance.completedQuests.Contains(GameManager.instance.acceptedQuests[i]) == false)
            {
                dm.DState = DialogueManager.DialogueState.QuestComplete;
                GameManager.instance.coin += GameManager.instance.acceptedQuests[i].coins;
                GameManager.instance.completedQuests.Add(GameManager.instance.acceptedQuests[i]);
                break;
            }
        }

        // index of who is currently speaking
        currentSpeaker = 0;
        numSpeakers = 0;
    }

    // Update is called once per frame
    void Update()
    {
        // check which state the dialogue is in: dialogue about the quest, dialogue about the reward, or normal dialogue
        switch (dm.DState)
        {
            case DialogueManager.DialogueState.QuestInfo:
                UpdateText(dm.QuestInfo);
                break;
            case DialogueManager.DialogueState.QuestComplete:
                UpdateText(dm.QuestCompleteInfo);
                break;
            case DialogueManager.DialogueState.Normal:
                UpdateText(dm.TextInfo);
                break;
            default:
                break;
        }
    }

    private void UpdateText(List<DialogueManager.CharacterInfo> characterTextInfos)
    {
        // update dialogue information
        if (Input.GetButtonDown("Submit") && startDialogue == false && finishText == true && gameObject.activeInHierarchy == true)
        {
            dialogueText.text = "";
            if (index < characterTextInfos.Count - 1)
            {
                index++;
                startDialogue = true;
            }
            else if (index >= characterTextInfos.Count - 1)
            {
                index = 0;
                startDialogue = false;
                for (int i = 0; i < characterIconBox.Count; i++)
                {
                    characterIconBox[i].SetActive(false);
                }
                if (dm.DState == DialogueManager.DialogueState.QuestInfo)
                {
                    qm.quest.Add(dm.quest);
                }
                gameObject.SetActive(false);
                dm.DState = DialogueManager.DialogueState.Normal;
                GameManager.instance.pc.enabled = true;
                GameManager.instance.pc.NPC.GetComponent<OverworldMovementInformation>().enabled = true;
            }
            finishText = false;
        }

        // story dialogue and normal dialogue has different layouts
        if (dm.importantText == true)
        {
            dialogueBackground.SetActive(true);

            // check if the next person speaking is a new speaker and only display one image of them
            bool newSpeaker = true;
            for (int i = 0; i < index; i++)
            {
                if (characterTextInfos[index].characterName == characterTextInfos[i].characterName && newSpeaker)
                {
                    newSpeaker = false;
                    currentSpeaker = i; // found index of the old image
                }
            }
            if (newSpeaker) // found a new speaker, display a new image
            {
                try
                {
                    characterIconBox[index].SetActive(true);
                    characterIconBox[index].GetComponent<Image>().sprite = characterTextInfos[index].characterIcon;
                    currentSpeaker = index; // current speaker is the current index
                }
                catch (System.Exception)
                {
                    Debug.Log("DialogueSystem: Caught a bug with displaying characters. Needs fixing in the UpdateText method.");
                }
            }
        }
        else
        {
            dialogueBackground.SetActive(false);
        }

        NameTextBox.text = characterTextInfos[index].characterName;
        if (textDisplaying == false && finishText == false && gameObject.activeInHierarchy == true)
        {
            textDisplaying = true;
            StartCoroutine(ShowText(characterTextInfos[index].characterDialogue));
        }
        startDialogue = false;

        // Make obvious which character is currently talking
        for (int i = 0; i < characterIconBox.Count; i++)
        {
            if (i != currentSpeaker)
            {
                characterIconBox[i].GetComponent<Image>().color = new Color(0.5f, 0.5f, 0.5f, 1f);
            }
            else if (i == currentSpeaker)
            {
                characterIconBox[currentSpeaker].GetComponent<Image>().color = Color.white;
            }
        }
    }

    // show text letter by letter
    private IEnumerator ShowText(string s)
    {
        int i = 0;
        dialogueTextBox.GetComponent<Image>().sprite = loadingImage;
        while (true)
        {
            if (i == s.Length)
            {
                textDisplaying = false;
                finishText = true;
                dialogueTextBox.GetComponent<Image>().sprite = doneImage;
                break;
            }
            dialogueText.text += s[i++];
            yield return new WaitForSeconds(0.02f);
        }
    }
}
