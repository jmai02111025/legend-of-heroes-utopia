using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class StatsMenu : MonoBehaviour
{
    private int currentCharacter;

    private int currentEquipment;

    private int currentSelectedEquipment;

    private Character character;

    [SerializeField]
    private TextMeshProUGUI nameText;

    [SerializeField]
    private Image elementIcon;

    [SerializeField]
    private TextMeshProUGUI HPValue;

    [SerializeField]
    private GameObject HPBar;

    [SerializeField]
    private GameObject MPBar;

    [SerializeField]
    private TextMeshProUGUI MPValue;

    [SerializeField]
    private TextMeshProUGUI STRValue;

    [SerializeField]
    private TextMeshProUGUI DEFValue;

    [SerializeField]
    private TextMeshProUGUI MGSValue;

    [SerializeField]
    private TextMeshProUGUI MDFValue;

    [SerializeField]
    private TextMeshProUGUI SPDValue;

    [SerializeField]
    private TextMeshProUGUI DEXValue;

    [SerializeField]
    private TextMeshProUGUI AGLValue;

    [SerializeField]
    private TextMeshProUGUI EVAValue;

    [SerializeField]
    private TextMeshProUGUI FireValue;

    [SerializeField]
    private TextMeshProUGUI WaterValue;

    [SerializeField]
    private TextMeshProUGUI EarthValue;

    [SerializeField]
    private TextMeshProUGUI WindValue;

    [SerializeField]
    private TextMeshProUGUI HolyValue;

    [SerializeField]
    private TextMeshProUGUI DarknessValue;

    [SerializeField]
    private List<TextMeshProUGUI> EquipmentText;

    [SerializeField]
    private List<Image> EquipmentPanel;

    [SerializeField]
    private GameObject ReplacementPanel;

    [SerializeField]
    private GameObject InventoryEquipmentPanel;

    private List<GameObject> UIList = new List<GameObject>();

    private TextMeshProUGUI[] newText;

    private bool added = false;

    private bool addToList = false;

    [SerializeField]
    private TextMeshProUGUI currentEquipmentName;

    [SerializeField]
    private TextMeshProUGUI currentEquipmentDescription;

    [SerializeField]
    private TextMeshProUGUI currentReplacementName;

    [SerializeField]
    private TextMeshProUGUI currentReplacementDescription;

    private void OnEnable()
    {
        currentCharacter = 0;
        currentEquipment = 0;
        currentSelectedEquipment = 0;
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        // set stat menu to stats of corresponding character
        character = GameManager.instance.Characters[currentCharacter];
        nameText.text = character.Base.CharacterName;
        elementIcon.sprite = character.Base.ElementIcon;
        HPValue.text = character.HP.ToString() + " / " + character.MaxHP.ToString();
        MPValue.text = character.MP.ToString() + " / " + character.MaxMP.ToString();
        HPBar.transform.localScale = new Vector3((float)character.HP / character.MaxHP, 1f);
        MPBar.transform.localScale = new Vector3((float)character.MP / character.MaxMP, 1f);
        STRValue.text = character.Str.ToString();
        DEFValue.text = character.Def.ToString();
        MGSValue.text = character.Mgs.ToString();
        MDFValue.text = character.Mdf.ToString();
        SPDValue.text = character.Spd.ToString();
        DEXValue.text = character.DEX.ToString() + "%";
        AGLValue.text = character.AGL.ToString() + "%";
        EVAValue.text = character.EVA.ToString() + "%";
        FireValue.text = (character.fireResist * 100).ToString() + "%";
        WaterValue.text = (character.waterResist * 100).ToString() + "%";
        EarthValue.text = (character.earthResist * 100).ToString() + "%";
        WindValue.text = (character.windResist * 100).ToString() + "%";
        HolyValue.text = (character.holyResist * 100).ToString() + "%";
        DarknessValue.text = (character.darknessResist * 100).ToString() + "%";

        // show current selected equipment
        for (int i = 0; i < EquipmentPanel.Count; i++)
        {
            if (i == currentEquipment)
            {
                EquipmentPanel[i].color = new Color(255f, 255f, 255f, 1f);
                Equipment.EquipmentType a = (Equipment.EquipmentType)i;
                foreach (Equipment item in character.Equipments)
                {
                    if (item.equipmentType == a)
                    {
                        currentEquipmentName.text = item.itemName;
                        currentEquipmentDescription.text = item.description;
                        break;
                    }
                    else
                    {
                        currentEquipmentName.text = "";
                        currentEquipmentDescription.text = "";
                    }
                }
            }
            else if (i != currentEquipment)
            {
                EquipmentPanel[i].color = new Color(255f, 255f, 255f, 0f);
            }
        }

        // display equipment name
        foreach (Equipment equipment in character.Equipments)
        {
            switch (equipment.equipmentType)
            {
                case Equipment.EquipmentType.Weapon:
                    EquipmentText[0].text = equipment.itemName;
                    break;
                case Equipment.EquipmentType.Armor:
                    EquipmentText[1].text = equipment.itemName;
                    break;
                case Equipment.EquipmentType.Shoes:
                    EquipmentText[2].text = equipment.itemName;
                    break;
                case Equipment.EquipmentType.Extra1:
                    EquipmentText[3].text = equipment.itemName;
                    break;
                case Equipment.EquipmentType.Extra2:
                    EquipmentText[4].text = equipment.itemName;
                    break;
                default:
                    break;
            }
        }

        // choose equipments
        if (InventoryEquipmentPanel.activeInHierarchy == false)
        {
            if (Input.GetKeyDown(KeyCode.RightArrow) && currentCharacter < GameManager.instance.Characters.Count - 1)
            {
                currentCharacter++;
                currentEquipment = 0;
                foreach (TextMeshProUGUI item in EquipmentText)
                {
                    item.text = "";
                }
            }
            if (Input.GetKeyDown(KeyCode.LeftArrow) && currentCharacter > 0)
            {
                currentCharacter--;
                currentEquipment = 0;
                foreach (TextMeshProUGUI item in EquipmentText)
                {
                    item.text = "";
                }
            }
            if (Input.GetKeyDown(KeyCode.DownArrow) && currentEquipment < EquipmentText.Count - 1)
            {
                currentEquipment++;
            }
            if (Input.GetKeyDown(KeyCode.UpArrow) && currentEquipment > 0)
            {
                currentEquipment--;
            }
            if (Input.GetButtonDown("Submit") && currentEquipment > 0)
            {
                InventoryEquipmentPanel.SetActive(true);
                ReplacementPanel.SetActive(true);
            }
            if (Input.GetButtonDown("Cancel"))
            {
                gameObject.SetActive(false);
                Time.timeScale = 1f;
            }
            if (Input.GetKeyDown(KeyCode.E))
            {
                switch (currentEquipment)
                {
                    case 1:
                        for (int i = 0; i < character.Equipments.Count; i++)
                        {
                            if (character.Equipments[i].equipmentType == Equipment.EquipmentType.Armor)
                            {
                                GameManager.instance.inventory.AddEquipment(character.Equipments[i], 1);
                                character.Equipments.RemoveAt(i);
                                EquipmentText[i].text = "";
                            }
                        }
                        break;
                    default:
                        break;
                }
            }
        }

        // equipment inventory
        else if (InventoryEquipmentPanel.activeInHierarchy == true)
        {
            AddItems();
            for (int i = 0; i < GameManager.instance.inventory.equipmentList.Count; i++)
            {
                if (GameManager.instance.inventory.equipmentList[i].itemName == UIList[currentSelectedEquipment].GetComponentsInChildren<TextMeshProUGUI>()[0].text)
                {
                    currentReplacementName.text = GameManager.instance.inventory.equipmentList[i].itemName;
                    currentReplacementDescription.text = GameManager.instance.inventory.equipmentList[i].description;
                }
            }

            if (Input.GetKeyDown(KeyCode.DownArrow) && currentSelectedEquipment < UIList.Count - 1)
            {
                currentSelectedEquipment++;
            }
            if (Input.GetKeyDown(KeyCode.UpArrow) && currentSelectedEquipment > 0)
            {
                currentSelectedEquipment--;
            }

            for (int i = 0; i < UIList.Count; i++)
            {
                newText = UIList[i].GetComponentsInChildren<TextMeshProUGUI>();
                if (i == currentSelectedEquipment)
                {
                    newText[0].color = Color.blue;
                }
                else
                {
                    newText[0].color = Color.black;
                }

                switch (currentEquipment)
                {
                    case 1:
                        GetEquipment(Equipment.EquipmentType.Armor);
                        break;
                    case 2:
                        GetEquipment(Equipment.EquipmentType.Shoes);
                        break;
                    case 3:
                        GetEquipment(Equipment.EquipmentType.Extra1);
                        break;
                    case 4:
                        GetEquipment(Equipment.EquipmentType.Extra2);
                        break;
                    default:
                        break;
                }
            }

            // close equipment inventory
            if (Input.GetButtonDown("Cancel"))
            {
                CloseEquipmentInventory();
                addToList = false;
            }

            // change equipment
            if (Input.GetButtonDown("Submit"))
            {
                added = false;
                addToList = false;
                for (int i = 0; i < GameManager.instance.inventory.equipmentList.Count; i++)
                {
                    if (GameManager.instance.inventory.equipmentList[i].itemName == UIList[currentSelectedEquipment].GetComponentsInChildren<TextMeshProUGUI>()[0].text)
                    {
                        for (int j = 0; j < character.Equipments.Count; j++)
                        {
                            if (character.Equipments[j].equipmentType == GameManager.instance.inventory.equipmentList[i].equipmentType)
                            {
                                GameManager.instance.inventory.AddEquipment(character.Equipments[j], 1);
                                character.Equipments[j] = GameManager.instance.inventory.equipmentList[i];
                                added = true;
                                break;
                            }
                        }

                        if (added == false)
                        {
                            character.Equipments.Add(GameManager.instance.inventory.equipmentList[i]);
                        }

                        GameManager.instance.inventory.UseEquipment(GameManager.instance.inventory.equipmentList[i]);
                        CloseEquipmentInventory();
                    }
                }
            }
        }
    }

    // get all equipments that has the same type as the one you selected
    private void GetEquipment(Equipment.EquipmentType type)
    {
        added = false;
        for (int i = 0; i < GameManager.instance.inventory.equipmentList.Count; i++)
        {
            if (GameManager.instance.inventory.equipmentList[i].equipmentType == type)
            {
                foreach (GameObject item in UIList)
                {
                    if (item.GetComponentsInChildren<TextMeshProUGUI>()[0].text == GameManager.instance.inventory.equipmentList[i].itemName)
                    {
                        added = true;
                        break;
                    }
                }
                if (added == false)
                {
                    newText[0].text = GameManager.instance.inventory.equipmentList[i].itemName;
                    newText[1].text = "x " + GameManager.instance.inventory.inventory[GameManager.instance.inventory.equipmentList[i]];
                }
            }
        }
    }

    // close equipment inventory menu
    private void CloseEquipmentInventory()
    {
        InventoryEquipmentPanel.SetActive(false);
        ReplacementPanel.SetActive(false);
        for (int i = 0; i < UIList.Count; i++)
        {
            newText = UIList[i].GetComponentsInChildren<TextMeshProUGUI>();
            newText[0].text = "";
            newText[1].text = "";
            currentReplacementName.text = "";
            currentReplacementDescription.text = "";
            currentSelectedEquipment = 0;
        }
        UIList.Clear();
    }

    private void AddItems()
    {
        if (addToList == false)
        {
            foreach (GameObject item in GameObject.FindGameObjectsWithTag("InventoryUI"))
            {
                UIList.Add(item);
            }
        }
        addToList = true;
    }
}
