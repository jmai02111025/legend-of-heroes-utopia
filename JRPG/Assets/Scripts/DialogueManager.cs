using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogueManager : MonoBehaviour
{
    // Create an instance of this whenever creating a new text
    [System.Serializable]
    public class CharacterInfo
    {
        public string characterName;
        public Sprite characterIcon;
        [TextArea] public string characterDialogue;
    }

    // Different states of dialogue
    public enum DialogueState
    {
        QuestInfo,
        QuestComplete,
        Normal,
    }

    public DialogueState DState;

    // tell player to accept quest
    public List<CharacterInfo> QuestInfo = new List<CharacterInfo>();

    // reward the player for completing the quest
    public List<CharacterInfo> QuestCompleteInfo = new List<CharacterInfo>();

    // normal text
    public List<CharacterInfo> TextInfo = new List<CharacterInfo>();

    public bool importantText;

    public Quest quest;
}
