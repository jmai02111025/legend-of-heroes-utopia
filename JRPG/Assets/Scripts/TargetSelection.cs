using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TargetSelection : MonoBehaviour
{
    #region Initial Variables
    public int numberOfEnemies = -1;

    [SerializeField]
    private BattleSystem bs;

    [SerializeField]
    private GameObject enemyStats;

    private int currentSelection;

    public bool finishedSelection;

    [SerializeField]
    private Text nameText;

    [SerializeField]
    private Text hpText;

    [SerializeField]
    private Text mpText;

    [SerializeField]
    private Text typeText;

    private Character enemy;

    private Character player;

    public bool attack;
    public WhichTarget target;

    [SerializeField]
    private ItemPanel ip;

    private Consumable item;
    #endregion

    // set the target to the first target according to the enemies' spawn positions

    private void OnEnable()
    {
        finishedSelection = false;
        currentSelection = 0;
        if (bs.state == BattleState.PlayerAction)
        {
            while (bs.battleSprites[currentSelection + bs.playerUnits.Count] == null)
            {
                currentSelection++;
            }
            transform.localPosition = bs.enemySpawnPositions[currentSelection] + new Vector3(0f, 120f, 0f);
        }
    }

    private void OnDisable()
    {
        enemyStats.SetActive(false);
    }

    private void Update()
    {
        if (attack == true)
        {
            if (target == WhichTarget.Enemies)
            {
                #region Attack Variables
                enemyStats.SetActive(true);
                enemy = bs.enemyUnits[currentSelection];
                nameText.text = bs.enemyUnits[currentSelection].Base.name;
                hpText.text = "HP: " + enemy.HP + "/ " + enemy.MaxHP;
                mpText.text = "MP: " + enemy.MP + "/ " + enemy.MaxMP;
                typeText.text = "" + enemy.Base.Type;
                // control to choose target

                if (Input.GetKeyDown(KeyCode.RightArrow) || Input.GetKeyDown(KeyCode.UpArrow))
                {
                    Debug.Log(target);
                    if (currentSelection < numberOfEnemies - 1)
                    {
                        currentSelection++;
                    }
                    else
                    {
                        currentSelection = 0;
                    }
                    while (bs.enemyUnits[currentSelection].HP <= 0)
                    {
                        currentSelection++;
                        if (currentSelection >= numberOfEnemies)
                        {
                            currentSelection = 0;
                        }
                    }
                }
                if (Input.GetKeyDown(KeyCode.LeftArrow) || Input.GetKeyDown(KeyCode.DownArrow))
                {
                    if (currentSelection > 0)
                    {
                        if (currentSelection > 0)
                        {
                            currentSelection--;
                        }
                        else
                        {
                            currentSelection = numberOfEnemies - 1;
                        }
                        while (bs.enemyUnits[currentSelection].HP <= 0)
                        {
                            currentSelection--;
                            if (currentSelection < 0)
                            {
                                currentSelection = numberOfEnemies - 1;
                            }
                        }
                    }
                    // target chosen
                    #endregion
                }
                transform.localPosition = new Vector3(bs.enemyUnits[currentSelection].xPos, bs.enemyUnits[currentSelection].yPos, 0f) + new Vector3(0f, 120f, 0f);
                
                if (Input.GetButtonDown("Submit"))
                {
                    finishedSelection = true;
                    enemyStats.SetActive(false);
                    for (int i = 0; i < bs.actionText.Count; i++)
                    {
                        bs.actionText[i].color = Color.white;
                    }
                    bs.state = BattleState.PlayerMove;
                }
            }
            if (target == WhichTarget.Allies)
            {
                #region Attack Variables
                enemyStats.SetActive(true);
                player = bs.playerUnits[currentSelection];
                nameText.text = bs.playerUnits[currentSelection].Base.name;
                hpText.text = "HP: " + player.HP + "/ " + player.MaxHP;
                mpText.text = "MP: " + player.MP + "/ " + player.MaxMP;
                typeText.text = "" + player.Base.Type;
                // control to choose target

                if (Input.GetKeyDown(KeyCode.RightArrow) || Input.GetKeyDown(KeyCode.UpArrow))
                {
                    if (currentSelection < bs.playerUnits.Count - 1)
                    {
                        currentSelection++;
                    }
                    else
                    {
                        currentSelection = 0;
                    }
                    while (bs.playerUnits[currentSelection].HP <= 0)
                    {
                        currentSelection++;
                        if (currentSelection >= numberOfEnemies)
                        {
                            currentSelection = 0;
                        }
                    }
                }
                if (Input.GetKeyDown(KeyCode.LeftArrow) || Input.GetKeyDown(KeyCode.DownArrow))
                {
                    if (currentSelection > 0)
                    {
                        if (currentSelection > 0)
                        {
                            currentSelection--;
                        }
                        else
                        {
                            currentSelection = bs.playerUnits.Count - 1;
                        }
                        while (bs.playerUnits[currentSelection].HP <= 0)
                        {
                            currentSelection--;
                            if (currentSelection < 0)
                            {
                                currentSelection = bs.playerUnits.Count - 1;
                            }
                        }
                    }
                    // target chosen
                    #endregion
                }

                transform.localPosition = new Vector3(bs.playerUnits[currentSelection].xPos, bs.playerUnits[currentSelection].yPos, 0f) + new Vector3(0f, 120f, 0f);

                // target chosen
                if (Input.GetButtonDown("Submit"))
                {
                    bs.actionButton[bs.currentSelection].GetComponent<Image>().sprite = bs.unselectedButton;
                    bs.actionText[bs.currentSelection].GetComponent<Text>().color = Color.white;
                    bs.currentSelection = 0;
                    finishedSelection = true;
                    enemyStats.SetActive(false);
                    bs.state = BattleState.PlayerMove;
                    target = WhichTarget.Enemies;
                }
                
            }
            
            // use items
           

            }
        else if (attack == false)
        {
            #region Item Information
            // control to choose target
            if (Input.GetKeyDown(KeyCode.RightArrow) && currentSelection < bs.playerUnitPosition.Length - 1)
            {
                currentSelection++;
            }
            if (Input.GetKeyDown(KeyCode.LeftArrow) && currentSelection > 0)
            {
                currentSelection--;
            }
            if (Input.GetKeyDown(KeyCode.UpArrow) && currentSelection >= 3)
            {
                currentSelection -= 3;
            }
            if (Input.GetKeyDown(KeyCode.DownArrow) && currentSelection < bs.playerUnitPosition.Length - 3)
            {
                currentSelection += 3;
            }
            transform.localPosition = bs.playerSpawnPositions[currentSelection] + new Vector3(0f, 220f, 0f);

            // use item
            if (Input.GetButtonDown("Submit") && bs.playerUnitPosition[currentSelection] != null)
            {
                for (int j = 0; j < bs.playerUnits.Count; j++)
                {
                    if (bs.playerUnits[j] == bs.playerUnitPosition[currentSelection])
                    {
                        currentSelection = j;
                        break;
                    }
                }
                bs.actionButton[bs.currentSelection].GetComponent<Image>().sprite = bs.unselectedButton;
                bs.actionText[bs.currentSelection].GetComponent<Text>().color = Color.white;
                bs.currentSelection = 0;
                finishedSelection = true;
                gameObject.SetActive(false);
                for (int i = 0; i < GameManager.instance.inventory.consumableList.Count; i++)
                {
                    if (GameManager.instance.inventory.consumableList[i].itemName == ip.GetComponent<ItemPanel>().GetItem())
                    {
                        item = GameManager.instance.inventory.consumableList[i];
                    }
                }
                switch (item.consumableType)
                {
                    case Consumable.ConsumableType.HP:
                        bs.playerUnits[currentSelection].HP += item.power;
                        if (bs.playerUnits[currentSelection].HP > bs.playerUnits[currentSelection].MaxHP)
                        {
                            bs.playerUnits[currentSelection].HP = bs.playerUnits[currentSelection].MaxHP;
                        }
                        break;
                    case Consumable.ConsumableType.MP:
                        bs.playerUnits[currentSelection].MP += item.power;
                        if (bs.playerUnits[currentSelection].MP > bs.playerUnits[currentSelection].MaxMP)
                        {
                            bs.playerUnits[currentSelection].MP = bs.playerUnits[currentSelection].MaxMP;
                        }
                        break;
                    default:
                        break;
                }
                GameManager.instance.inventory.UseConsumable(item);
                bs.characterHuds[currentSelection].GetComponent<CharacterHud>().SetData(bs.playerUnits[currentSelection]);
                bs.state = BattleState.TurnRoll;
            }
        }
    }
    #endregion
    public int GetTarget()
        {
            return currentSelection;
        }

}
