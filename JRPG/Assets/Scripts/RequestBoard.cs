using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class RequestBoard : MonoBehaviour
{
    [SerializeField]
    private QuestManager qm;

    [SerializeField]
    private GameObject questPrefab;

    private List<Quest> quests = new List<Quest>();

    private List<GameObject> questObjects = new List<GameObject>();

    [SerializeField]
    private Transform parent;

    private TextMeshProUGUI[] text;

    [SerializeField]
    private int minX;

    [SerializeField]
    private int maxX;

    [SerializeField]
    private int minY;

    [SerializeField]
    private int maxY;

    private int currentQuest;

    [SerializeField]
    private GameObject questInfo;

    [SerializeField]
    private TextMeshProUGUI questName;

    [SerializeField]
    private TextMeshProUGUI questDescription;

    [SerializeField]
    private TextMeshProUGUI questReward;

    private bool infoPage;

    private void OnEnable()
    {
        currentQuest = 0;
        quests = qm.quest;
        // don't instantiate accepted quests
        for (int i = 0; i < quests.Count; i++)
        {
            for (int j = 0; j < GameManager.instance.acceptedQuests.Count; j++)
            {
                if (quests[i].questName == GameManager.instance.acceptedQuests[j].questName)
                {
                    quests.RemoveAt(i);
                }
            }
        }
        // create quest prefabs on enable, don't instantiate duplicates of the same quest
        while (questObjects.Count < quests.Count)
        {
            for (int i = 0; i < quests.Count; i++)
            {
                GameObject gb = Instantiate(questPrefab, new Vector3(0f, 0f, 0f), Quaternion.identity, parent);
                questObjects.Add(gb);
                gb.transform.localPosition = new Vector3(Random.Range(minX, maxX), Random.Range(minY, maxY), 0f);
                gb.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
                text = gb.GetComponentsInChildren<TextMeshProUGUI>();
                text[0].text = quests[i].questName.ToString();
            }
        }
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        // show which quest is current selected
        if (gameObject.activeInHierarchy == true)
        {
            for (int i = 0; i < questObjects.Count; i++)
            {
                if (i == currentQuest)
                {
                    questObjects[i].GetComponent<Image>().color = Color.Lerp(Color.white, Color.grey, Mathf.PingPong(Time.time, 1));
                }
                else if (i != currentQuest)
                {
                    questObjects[i].GetComponent<Image>().color = Color.white;
                }
            }

            // select quest
            if (Input.GetKeyDown(KeyCode.RightArrow) && infoPage == false)
            {
                if (currentQuest < questObjects.Count - 1)
                {
                    currentQuest++;
                }
                else if (currentQuest == questObjects.Count - 1)
                {
                    currentQuest = 0;
                }
            }
            if (Input.GetKeyDown(KeyCode.LeftArrow) && infoPage == false)
            {
                if (currentQuest > 0)
                {
                    currentQuest--;
                }
                else if (currentQuest == 0)
                {
                    currentQuest = questObjects.Count - 1;
                }
            }

            if (Input.GetButtonDown("Submit"))
            {
                // see more info about the selected quest
                if (infoPage == false && quests.Count != 0)
                {
                    questInfo.SetActive(true);
                    infoPage = true;
                    questName.text = quests[currentQuest].questName.ToString();
                    questDescription.text = quests[currentQuest].description;
                    if (quests[currentQuest].coins != 0)
                    {
                        questReward.text = quests[currentQuest].coins + " Coins";
                    }
                }
                // accpet quest
                else if (infoPage == true)
                {
                    GameManager.instance.acceptedQuests.Add(quests[currentQuest]);
                    qm.quest.RemoveAt(currentQuest);
                    Destroy(questObjects[currentQuest]);
                    questObjects.RemoveAt(currentQuest);
                    questInfo.SetActive(false);
                    infoPage = false;
                    currentQuest = 0;
                }
            }
            if (Input.GetButtonDown("Cancel"))
            {
                // close info page
                if (infoPage == true)
                {
                    questInfo.SetActive(false);
                    infoPage = false;
                }
                // close request board
                else if (infoPage == false)
                {
                    gameObject.SetActive(false);
                    GameManager.instance.pc.enabled = true;
                }
            }
        }
    }
}
