using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ShopManager : MonoBehaviour
{
    [SerializeField]
    private List<Consumable> consumables;

    [SerializeField]
    private List<Equipment> equipments;

    [SerializeField]
    private GameObject shopPanel;

    public bool canShop = false;

    [SerializeField]
    private ShopItem si;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (canShop == true)
        {
            if (Input.GetButtonDown("Submit"))
            {
                shopPanel.SetActive(true);
                si.GetComponent<ShopItem>().GetItems(consumables, equipments);
                GameManager.instance.pc.enabled = false;
            }
            if (Input.GetButtonDown("Cancel"))
            {
                shopPanel.SetActive(false);
                GameManager.instance.pc.enabled = true;
            }
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            canShop = true;
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            canShop = false;
        }
    }
}
