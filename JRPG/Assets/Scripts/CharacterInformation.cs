using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterInformation
{
    public Character characterInformation;

    public  GameObject characterSprite;

    public GameObject characterHud;

    public Vector3 spawnPosition;

    public bool isAlly = false;
}
