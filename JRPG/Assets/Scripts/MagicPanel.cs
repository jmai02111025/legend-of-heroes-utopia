using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MagicPanel : MonoBehaviour
{
    [SerializeField]
    private BattleSystem bs;

    [SerializeField]
    private List<Image> magicSlot;

    [SerializeField]
    private List<Text> magicText;

    [SerializeField]
    private List<Text> costText1;

    [SerializeField]
    private List<Text> costText2;

    [SerializeField]
    private Text description;

    private int currentMagic;

    private Character character;

    private void OnEnable()
    {
        currentMagic = 0;
        character = bs.listOfAllCharacters[bs.moveIndex].characterInformation;
        // set all the data for the magics on enable
        for (int i = 0; i < character.Magics.Count; i++)
        {
            magicText[i].text = character.Magics[i].Base.MagicName;
        }
        for (int i = 0; i < character.Magics.Count; i++)
        {
            costText1[i].text = character.Magics[i].Base.Cost.ToString();
        }
        for (int i = 0; i < character.Magics.Count; i++)
        {
            costText2[i].text = character.Magics[i].Base.Cost.ToString();
        }
    }

    // reset magic panel
    private void OnDisable()
    {
        magicSlot[currentMagic].color = new Color(0f, 0f, 0f, 0.5f);

        // reset all the data for the magics on disable
        for (int i = 0; i < character.Magics.Count; i++)
        {
            magicText[i].text = "";
        }
        for (int i = 0; i < character.Magics.Count; i++)
        {
            costText1[i].text = "";
        }
        for (int i = 0; i < character.Magics.Count; i++)
        {
            costText2[i].text = "";
        }
    }

    private void Update()
    {
        if (character.Magics.Count != 0)
        {
            MagicSelection();
        }
    }

    // function to choose magic
    private void MagicSelection()
    {
        magicSlot[currentMagic].color = Color.Lerp(Color.white, Color.yellow, Mathf.PingPong(Time.time, 1));
        description.text = "This magic deals " + character.Magics[currentMagic].Power + " " + character.Magics[currentMagic].Base.BType + " " + character.Magics[currentMagic].Base.DType + " damage";

        if (Input.GetKeyDown(KeyCode.RightArrow) && currentMagic < character.Magics.Count - 1)
        {
            magicSlot[currentMagic].color = new Color(0f, 0f, 0f, 0.5f);
            currentMagic++;
        }
        if (Input.GetKeyDown(KeyCode.LeftArrow) && currentMagic > 0)
        {
            magicSlot[currentMagic].color = new Color(0f, 0f, 0f, 0.5f);
            currentMagic--;
        }
        if (Input.GetKeyDown(KeyCode.UpArrow) && currentMagic > 1)
        {
            magicSlot[currentMagic].color = new Color(0f, 0f, 0f, 0.5f);
            currentMagic -= 2;
        }
        if (Input.GetKeyDown(KeyCode.DownArrow) && currentMagic < character.Magics.Count - 2)
        {
            magicSlot[currentMagic].color = new Color(0f, 0f, 0f, 0.5f);
            currentMagic += 2;
        }
        if (Input.GetButtonDown("Submit") && character.Magics.Count != 0)
        {
            if (character.MP >= character.Magics[currentMagic].Base.Cost)
            {
                bs.turnIndicator.SetActive(false);
                bs.actionSeletor.SetActive(false);
                bs.targetSelector.SetActive(true);
                bs.targetSelector.GetComponent<TargetSelection>().attack = true;
                gameObject.SetActive(false);
            }
            else
            {
                Debug.Log("Not enough MP");
            }
        }
    }

    public int GetMagic()
    {
        return currentMagic;
    }
}
