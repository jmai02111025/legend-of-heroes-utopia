using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory
{
    public Dictionary<Item, int> inventory;

    public List<Consumable> consumableList;

    public List<Equipment> equipmentList;

    public Inventory()
    {
        inventory = new Dictionary<Item, int>();
        consumableList = new List<Consumable>();
        equipmentList = new List<Equipment>();
    }

    // add item to the list if item is not already there
    // if item is already there, add amount
    public void AddConsumable(Consumable item, int amount)
    {
        if (!inventory.ContainsKey(item))
        {
            inventory.Add(item, amount);
            consumableList.Add(item);
        }
        else if (inventory.ContainsKey(item))
        {
            inventory[item] += amount;
        }
    }

    // remove item if it's the last item
    // amount minus 1 if it's not the last item
    public void UseConsumable(Consumable item)
    {
        if (inventory[item] == 1)
        {
            inventory.Remove(item);
            consumableList.Remove(item);
        }
        else if (inventory[item] > 1)
        {
            inventory[item] -= 1;
        }
    }

    // add equipment to the list
    public void AddEquipment(Equipment item, int amount)
    {
        if (!inventory.ContainsKey(item))
        {
            inventory.Add(item, amount);
            equipmentList.Add(item);
        }
        else if (inventory.ContainsKey(item))
        {
            inventory[item] += amount;
        }
    }

    // use an equipment
    public void UseEquipment(Equipment item)
    {
        if (inventory[item] == 1)
        {
            inventory.Remove(item);
            equipmentList.Remove(item);
        }
        else if (inventory[item] > 1)
        {
            inventory[item] -= 1;
        }
    }
}
