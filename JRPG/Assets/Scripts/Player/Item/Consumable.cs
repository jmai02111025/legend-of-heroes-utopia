using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
[CreateAssetMenu(fileName = "DefaultConsumable", menuName = "ScriptableObjects/Items/Consumable")]
public class Consumable : Item
{
    public enum ConsumableType
    {
        HP,
        MP,
    }

    public enum Target
    {
        Self,
        Enemy,
    }

    public ConsumableType consumableType;

    public Target target;

    public int power;
}
