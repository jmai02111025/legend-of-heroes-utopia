using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public abstract class Item : ScriptableObject
{
    public string itemName;

    [TextArea]
    public string description;

    public int cost;

    public enum ItemType
    {
        Consumable,
        Equipment,
    }

    public ItemType itemType;
}
