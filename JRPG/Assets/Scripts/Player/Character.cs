using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character
{
    #region Character Information
    public CharacterBase Base { get; set; }
    public int HP { get; set; }
    public int MP { get; set; }
    public int StrFamiliarity { get; set; }
    public int DefFamiliarity { get; set; }
    public int MgsFamiliarity { get; set; }
    public int MdfFamiliarity { get; set; }
    public int WeaponFamiliarity { get; set; }
    public int StrBonus { get; set; }
    public int DefBonus { get; set; }
    public int MgsBonus { get; set; }
    public int MdfBonus { get; set; }
    public float fireResist { get; set; }
    public float waterResist { get; set; }
    public float earthResist { get; set; }
    public float windResist { get; set; }
    public float holyResist { get; set; }
    public float darknessResist { get; set; }

    public List<Magic> Magics { get; set; }

    public List<Equipment> Equipments { get; set; }

    public int characterInt = -1;

    public float xPos;
    public float yPos;

    public bool isConfused;
    public bool isReflected;

    #endregion
    // character constructor
    public Character(CharacterBase ABase)
    {
        #region Character Stats
        Equipments = new List<Equipment>();
        Base = ABase;
        HP = MaxHP;
        MP = MaxMP;

        StrFamiliarity = GetFamiliarity(Str);
        DefFamiliarity = GetFamiliarity(Def);
        MgsFamiliarity = GetFamiliarity(Mgs);
        MdfFamiliarity = GetFamiliarity(Mdf);
        WeaponFamiliarity = 0;

        StrBonus = 0;
        DefBonus = 0;
        MgsBonus = 0;
        MdfBonus = 0;

        // float based percentage
        fireResist = 0;
        waterResist = 0;
        earthResist = 0;
        windResist = 0;
        holyResist = 0;
        darknessResist = 0;
        #endregion
        Magics = new List<Magic>();
        GetMagic();
    }
    #region Character Stats
    public int Str
    {
        get
        {
            int a = 0;
            foreach (Equipment e in Equipments)
            {
                if (e.equipmentType == Equipment.EquipmentType.Weapon)
                {
                    a += e.STR;
                }
            }
            return Mathf.FloorToInt((Base.Str) * ((float)HP / MaxHP) + a + StrBonus);
        }
    }

    public int Def
    {
        get
        {
            int a = 0;
            foreach (Equipment e in Equipments)
            {
                if (e.equipmentType == Equipment.EquipmentType.Armor)
                {
                    a += e.DEF;
                }
            }
            return Mathf.FloorToInt((Base.Def) * ((float)HP / MaxHP) + a + MdfBonus);
        }
    }

    public int Mgs
    {
        get
        {
            int a = 0;
            foreach (Equipment e in Equipments)
            {
                if (e.equipmentType == Equipment.EquipmentType.Weapon)
                {
                    a += e.MGS;
                }
            }
            return Mathf.FloorToInt((Base.Mgs) * ((float)MP / MaxMP) + a + MgsBonus);
        }
    }

    public int Mdf
    {
        get
        {
            return Mathf.FloorToInt((Base.Mdf) * ((float)MP / MaxMP) + MdfBonus);
        }
    }

    public int Spd
    {
        get
        {
            int a = 0;
            foreach (Equipment e in Equipments)
            {
                if (e.equipmentType == Equipment.EquipmentType.Shoes)
                {
                    a += e.SPD;
                }
            }
            return Mathf.FloorToInt((Base.Spd) * ((float)HP / MaxHP) + a);
        }
    }

    public int DEX
    {
        get
        {
            return Base.Dex;
        }
    }

    public int AGL
    {
        get
        {
            return Base.Agl;
        }
    }

    public int EVA
    {
        get
        {
            return Base.Eva;
        }
    }

    public int MaxHP
    {
        get
        {
            return Mathf.FloorToInt((Base.Str + Base.Def + Base.Mgs + Base.Mdf) * 2);
        }
    }

    public int MaxMP
    {
        get
        {
            return Mathf.FloorToInt((Base.Mgs / 2));
        }
    }
    #endregion

    // When attacked using the attack function
    public void TakeAttackDamage(Character attacker)
    {
        Debug.Log("Attacker's str value: " + attacker.Str);
        Debug.Log("Target's def value: " + Def);

        // Hit?
        if (Hit(attacker.DEX, AGL) == true)
        {
            // Critical hit?
            float modifiers = 1f;
            if (CriticalHit(attacker.Base.Luck) == true)
            {
                modifiers = Random.Range(1.1f, 1.25f);
            }
            else
            {
                modifiers = Random.Range(0.85f, 1f);
            }

            int a = attacker.Str - Def;
            int damage = Mathf.FloorToInt(a * modifiers);
            if (damage <= 0)
            {
                damage = 1;
            }
            HP -= damage;
            if (HP <= 0)
            {
                HP = 0;
            }
            Debug.Log("Total damage: " + damage);
        }
    }

    // When attacked using the magic function
    public void TakeSkillDamage(Magic magic, Character attacker)
    {
        if (Hit(attacker.DEX, EVA) == true)
        {
            // Critical hit?
            float modifiers = 1f;
            if (CriticalHit(attacker.Base.Luck) == true)
            {
                modifiers = Random.Range(1.1f, 1.25f);
            }
            else
            {
                modifiers = Random.Range(0.85f, 1f);
            }
            int a = 0;
            float b = 0;
            float c = 0f;
            // check to see if the player has the same type as the magic being used
            if (attacker.Base.Type == magic.Base.BType)
            {
                b = 1.1f;
            }
            else if (attacker.Base.Type != magic.Base.BType)
            {
                b = 0.8f;
            }
            // magic that deals magic damage
            if (magic.Base.DType == DamageType.Magic)
            {
                a = attacker.Mgs - Mdf;
                Debug.Log("Attacker's mgs value: " + attacker.Mgs);
                Debug.Log("Target's mdf value " + Mdf);
            }
            // magic that deals physical damage
            if (magic.Base.DType == DamageType.Physical)
            {
                a = attacker.Str - Def;
                Debug.Log("Attacker's str value: " + attacker.Str);
                Debug.Log("Target's def value: " + Def);
            }
            c = CheckEffectiveness(Base.Type, magic);

            int damage = Mathf.FloorToInt(Reduction(magic) * Mathf.FloorToInt(a * modifiers * b * c) + (magic.Power * 10));
            if (damage <= 0)
            {
                damage = 1;
            }
            HP -= damage;
            if (HP <= 0)
            {
                HP = 0;
            }
            attacker.MP -= magic.Base.Cost;

            Debug.Log("Total damage: " + damage);
        }
    }

    // check if the magic type is effective against the target type
    private float CheckEffectiveness(BaseType type, Magic magic)
    {
        #region Magic Types
        switch (type)
        {
            case BaseType.Fire:
                if (magic.Base.BType == BaseType.Fire)
                {
                    return 0.8f;
                }
                if (magic.Base.BType == BaseType.Water)
                {
                    return 1.2f;
                }
                if (magic.Base.BType == BaseType.Wind)
                {
                    return 0.5f;
                }
                return 1f;
            case BaseType.Water:
                if (magic.Base.BType == BaseType.Water)
                {
                    return 0.8f;
                }
                if (magic.Base.BType == BaseType.Earth)
                {
                    return 1.2f;
                }
                if (magic.Base.BType == BaseType.Fire)
                {
                    return 0.5f;
                }
                return 1f;
            case BaseType.Earth:
                if (magic.Base.BType == BaseType.Earth)
                {
                    return 0.8f;
                }
                if (magic.Base.BType == BaseType.Wind)
                {
                    return 1.2f;
                }
                if (magic.Base.BType == BaseType.Water)
                {
                    return 0.5f;
                }
                return 1f;
            case BaseType.Wind:
                if (magic.Base.BType == BaseType.Wind)
                {
                    return 0.8f;
                }
                if (magic.Base.BType == BaseType.Fire)
                {
                    return 1.2f;
                }
                if (magic.Base.BType == BaseType.Earth)
                {
                    return 0.5f;
                }
                return 1f;
            case BaseType.Holy:
                if (magic.Base.BType == BaseType.Darkness)
                {
                    return 1.2f;
                }
                if (magic.Base.BType == BaseType.Holy)
                {
                    return 0.8f;
                }
                return 1f;
            case BaseType.Darkness:
                if (magic.Base.BType == BaseType.Holy)
                {
                    return 1.2f;
                }
                if (magic.Base.BType == BaseType.Darkness)
                {
                    return 0.8f;
                }
                return 1f;
            default:
                return 1f;
        }
        #endregion  
    }

    // Final damage calculation
    private float Reduction(Magic magic)
    {
        switch (magic.Base.BType)
        {
            case BaseType.Fire:
                return (1 - fireResist);
            case BaseType.Water:
                return (1 - waterResist);
            case BaseType.Earth:
                return (1 - earthResist);
            case BaseType.Wind:
                return (1 - windResist);
            case BaseType.Holy:
                return (1 - holyResist);
            case BaseType.Darkness:
                return (1 - darknessResist);
            default:
                return 1;
        }
    }

    // Determine critical hit
    private bool CriticalHit(int luck)
    {
        int random = Random.Range(1, 101);
        if (random <= luck)
        {
            Debug.Log("Critical");
            return true;
        }
        else
        {
            Debug.Log("Not Critical");
            return false;
        }
    }

    // Determine if the attack hit
    private bool Hit(int dex, int miss)
    {
        int chance = dex - miss;
        int random = Random.Range(1, 101);
        if (random <= chance)
        {
            Debug.Log("Hit");
            return true;
        }
        else
        {
            Debug.Log("Not Hit");
            return false;
        }
    }

    // Get familiarity
    public int GetFamiliarity(int stat)
    {
        return stat / 1000;
    }

    // Get Magic
    public void GetMagic()
    {
        foreach (UniqueMagic UniqueMagic in Base.UniqueMagics)
        {
            if (UniqueMagic.Level <= WeaponFamiliarity)
            {
                Magic m = new Magic(UniqueMagic.Base);
                if (Magics.Count == 0)
                {
                    Magics.Add(m);
                }
                else
                {
                    for (int i = 0; i < Magics.Count; i++)
                    {
                        if (Magics[i].Base != UniqueMagic.Base)
                        {
                            Magics.Add(m);
                            Debug.Log("New Magic Unlocked");
                        }
                    }
                }
            }
        }
    }
}
