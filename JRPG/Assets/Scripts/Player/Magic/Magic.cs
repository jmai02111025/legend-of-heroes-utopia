using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Magic
{
    public MagicBase Base { get; set; }

    public int magicFamiliarity { get; set; }

    public Magic(MagicBase MBase)
    {
        Base = MBase;
        magicFamiliarity = 0;
    }

    public int Power
    {
        get
        {
            return Base.BasePower + magicFamiliarity;
        }
    }
}
