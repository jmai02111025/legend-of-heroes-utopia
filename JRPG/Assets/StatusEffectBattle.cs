using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StatusEffectBattle : MonoBehaviour
{
    #region Initial Variables
    public BattleSystem battleSystem;
    public Dictionary<int,List<StatusEffectsScript.StatusEffectVariable>> statusEffectDictionary = new Dictionary<int, List<StatusEffectsScript.StatusEffectVariable>>();
    public Dictionary<int, Character> listOfCharacters = new Dictionary<int, Character>();
    #endregion

    //Assigns the battle system to the begining of turn delegate void
    void Start()
    {
        if (battleSystem != null)
        {
            battleSystem.beginingOfTurn += CheckStatusEffect;
        }
    }

    //This function assigns a new status effect or increases its durration 
    public void AssignNewStatusEffect(StatusCondition statusCondition, int characterInt)
    {
        //Creates new temperary variables that will be used for store the information before its added to the dictionaries
        StatusEffectsScript.StatusEffectVariable tempStatusEffectVariable = new StatusEffectsScript.StatusEffectVariable();
        List<StatusEffectsScript.StatusEffectVariable> tempStatusEffectList = new List<StatusEffectsScript.StatusEffectVariable>();
        //Checks to see if the character whoes turn it is had any status effects attached to them
        if (statusEffectDictionary.ContainsKey(characterInt))
        {
            //For each status condition attached to the character, loop through and see if the one being assigned already is assigned, and if it is, then increase its durration
            for (int i = 0; i < statusEffectDictionary[characterInt].Count; i++)
            {
                if (statusCondition == statusEffectDictionary[characterInt][i].statusEffect)
                {
                    switch (statusCondition)
                    {
                        case StatusCondition.None:
                            break;
                        case StatusCondition.Burn:
                            Debug.Log("Burn status effect");
                            statusEffectDictionary[characterInt][i].numberOfTurns += 2;
                            break;
                        case StatusCondition.HealOverTime:
                            statusEffectDictionary[characterInt][i].numberOfTurns += 2;
                            break;
                        case StatusCondition.Blind:
                            statusEffectDictionary[characterInt][i].numberOfTurns += 2;
                            break;
                        case StatusCondition.Confuse:
                            statusEffectDictionary[characterInt][i].numberOfTurns += 2;
                            break;
                        case StatusCondition.Reflect:
                            statusEffectDictionary[characterInt][i].numberOfTurns += 2;
                            break;

                    }
                    break;
                }
            }
           // statusEffectDictionary.Remove(characterInt);

            //If the status effect is not already afflicted, then it is added to the characters dictionary
            switch (statusCondition)
            {
                case StatusCondition.None:
                    break;
                case StatusCondition.Burn:
                    tempStatusEffectVariable.numberOfTurns = 2;
                    tempStatusEffectVariable.effectAmmount = 999999999;
                    tempStatusEffectVariable.statusEffect = StatusCondition.Burn;
                    tempStatusEffectList.Add(tempStatusEffectVariable);
                    statusEffectDictionary.Add(characterInt, tempStatusEffectList);
                    break;
                case StatusCondition.HealOverTime:
                    tempStatusEffectVariable.numberOfTurns = 2;
                    tempStatusEffectVariable.effectAmmount = 999999999;
                    tempStatusEffectVariable.statusEffect = StatusCondition.HealOverTime;
                    tempStatusEffectList.Add(tempStatusEffectVariable);
                    statusEffectDictionary.Add(characterInt, tempStatusEffectList);
                    break;
                case StatusCondition.Blind:
                    tempStatusEffectVariable.numberOfTurns = 2;
                    tempStatusEffectVariable.effectAmmount = 999999999;
                    tempStatusEffectVariable.statusEffect = StatusCondition.Blind;
                    tempStatusEffectList.Add(tempStatusEffectVariable);
                    statusEffectDictionary.Add(characterInt, tempStatusEffectList);
                    break;
                case StatusCondition.Confuse:
                    tempStatusEffectVariable.numberOfTurns = 2;
                    tempStatusEffectVariable.effectAmmount = 999999999;
                    tempStatusEffectVariable.statusEffect = StatusCondition.Confuse;
                    tempStatusEffectList.Add(tempStatusEffectVariable);
                    statusEffectDictionary.Add(characterInt, tempStatusEffectList);
                    break;
                case StatusCondition.Reflect:
                    tempStatusEffectVariable.numberOfTurns = 2;
                    tempStatusEffectVariable.effectAmmount = 999999999;
                    tempStatusEffectVariable.statusEffect = StatusCondition.Reflect;
                    tempStatusEffectList.Add(tempStatusEffectVariable);
                    statusEffectDictionary.Add(characterInt, tempStatusEffectList);
                    break;
            }


        }
        //If there are no status effects attached to the character, then assign the new effect to them, and create an entry in the dictionary
        switch (statusCondition)
        {
            case StatusCondition.None:
                break;
            case StatusCondition.Burn:
                tempStatusEffectVariable.numberOfTurns = 2;
                tempStatusEffectVariable.effectAmmount = 999999999;
                tempStatusEffectVariable.statusEffect = StatusCondition.Burn;
                tempStatusEffectList.Add(tempStatusEffectVariable);
                statusEffectDictionary.Add(characterInt, tempStatusEffectList);
                break;
            case StatusCondition.HealOverTime:
                tempStatusEffectVariable.numberOfTurns = 2;
                tempStatusEffectVariable.effectAmmount = 999999999;
                tempStatusEffectVariable.statusEffect = StatusCondition.HealOverTime;
                tempStatusEffectList.Add(tempStatusEffectVariable);
                statusEffectDictionary.Add(characterInt, tempStatusEffectList);
                break;
            case StatusCondition.Blind:
                tempStatusEffectVariable.numberOfTurns = 2;
                tempStatusEffectVariable.effectAmmount = 999999999;
                tempStatusEffectVariable.statusEffect = StatusCondition.Blind;
                tempStatusEffectList.Add(tempStatusEffectVariable);
                statusEffectDictionary.Add(characterInt, tempStatusEffectList);
                break;
            case StatusCondition.Confuse:
                tempStatusEffectVariable.numberOfTurns = 2;
                tempStatusEffectVariable.effectAmmount = 999999999;
                tempStatusEffectVariable.statusEffect = StatusCondition.Confuse;
                tempStatusEffectList.Add(tempStatusEffectVariable);
                statusEffectDictionary.Add(characterInt, tempStatusEffectList);
                break;
            case StatusCondition.Reflect:
                tempStatusEffectVariable.numberOfTurns = 2;
                tempStatusEffectVariable.effectAmmount = 999999999;
                tempStatusEffectVariable.statusEffect = StatusCondition.Reflect;
                tempStatusEffectList.Add(tempStatusEffectVariable);
                statusEffectDictionary.Add(characterInt, tempStatusEffectList);
                break;
        }

    }

    //Loops through at the begining of the turn, and for every effect attached to a character causes their effect to activate
    void CheckStatusEffect(int whichCharacterTakesDamage, int whichAlly)
    {
            if (statusEffectDictionary.ContainsKey(whichCharacterTakesDamage))
            {
                Character tempCharacter = listOfCharacters[whichCharacterTakesDamage];
                List<StatusEffectsScript.StatusEffectVariable> listOfCharactersStatusEffects = statusEffectDictionary[whichCharacterTakesDamage];
                if (tempCharacter.HP > 0)
                {
                    for (int i = 0; i < listOfCharactersStatusEffects.Count; i++)
                    {
                        switch (listOfCharactersStatusEffects[i].statusEffect)
                        {
                            case StatusCondition.None:
                                break;
                            case StatusCondition.Burn:
                                if (tempCharacter.HP - listOfCharactersStatusEffects[i].effectAmmount <= 0)
                                {
                                    battleSystem.enemyDied = true;
                                }
                                tempCharacter.HP -= listOfCharactersStatusEffects[i].effectAmmount;
                                break;
                            case StatusCondition.HealOverTime:
                                tempCharacter.HP += listOfCharactersStatusEffects[i].effectAmmount;
                                if (tempCharacter.HP > tempCharacter.MaxHP)
                                {
                                    tempCharacter.HP = tempCharacter.MaxHP;
                                }
                            battleSystem.characterHuds[whichAlly].GetComponent<CharacterHud>().SetData(battleSystem.playerUnits[whichAlly]);
                            break;
                            case StatusCondition.Blind:
                            if (listOfCharactersStatusEffects[i].numberOfTurns > 0)
                            {
                                listOfCharactersStatusEffects[i].numberOfTurns -= 1;
                                tempCharacter.Base.isBlind = true;
                            }
                            else
                            {
                                tempCharacter.Base.isBlind = false;

                            }
                                break;
                            case StatusCondition.Confuse:
                            if (listOfCharactersStatusEffects[i].numberOfTurns > 0)
                            {
                                listOfCharactersStatusEffects[i].numberOfTurns -= 1;
                                tempCharacter.isConfused = true;
                            }
                            else
                            {
                                tempCharacter.isConfused = false;

                            }
                            break;
                            case StatusCondition.Reflect:
                            if (listOfCharactersStatusEffects[i].numberOfTurns > 1)
                            {
                                listOfCharactersStatusEffects[i].numberOfTurns -= 1;
                                tempCharacter.isReflected = true;
                            }
                            else
                            {
                                tempCharacter.isReflected = false;

                            }
                            break;
                    }
                    }
                }
            }
    }
}
