using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

public class BuildScript
{
    [MenuItem("Build Menu/Build Client")]
    public static void BuildClient()
    {
        // Get the list of all the scenes in the build
        List<EditorBuildSettingsScene> scenes = new List<EditorBuildSettingsScene>(EditorBuildSettings.scenes);
        List<string> enabledScenes = new List<string>();
        foreach (EditorBuildSettingsScene scene in scenes)
        {
            if (scene.enabled)
            {
                enabledScenes.Add(scene.path);
            }
        }

        // Make the build
        string path = Application.dataPath + "/../Builds/" + Application.productName + ".exe";
        // How to define any custom scripting symbols:
        // PlayerSettings.SetScriptingDefineSymbolsForGroup(BuildTargetGroup.Standalone, "CLIENT");
        
        Debug.Log("Making build, saving to " + path);
        BuildPipeline.BuildPlayer(enabledScenes.ToArray(), path, BuildTarget.StandaloneWindows64, BuildOptions.None);
    }
}
